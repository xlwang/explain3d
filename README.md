# Explain3D

###This repository contains two pairs of Academic datasets.
1. NCES vs. UMass
2. NCES vs. OSU

The NCES dataset is downloaded from: https://nces.ed.gov

The UMass dataset is acquired from:  https://www.umass.edu/gateway/academics/undergraduate

The OSU dataset is acquired from: http://undergrad.osu.edu/majors-and-academics/majors

Note that the NCES dataset is simplified for the evaluation.